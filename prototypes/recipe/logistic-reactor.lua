local reactor_recipe = table.deepcopy(data.raw["recipe"]["nuclear-reactor"])

reactor_recipe.name = "logistic-reactor"
reactor_recipe.result = "logistic-reactor"
reactor_recipe.icon = "__logistics-reactor-next__/graphics/icons/logistic-reactor-recipe.png"
reactor_recipe.icon_size = 32

data:extend({
      {
         type = "recipe",
         name = "logistic-reactor",
         result = "logistic-reactor",
         icon = "__logistics-reactor-next__/graphics/icons/logistic-reactor-recipe.png",
         icon_size = 32,
         energy_required = 18,
         enabled = false,
         ingredients =
            {
               {"nuclear-reactor", 1},
               {"logistic-chest-passive-provider", 1},
               {"logistic-chest-requester", 1},
            },
         requester_paste_multiplier = 1
      }
})

-- also assumes Bob's Logistics is available if Bob's Power is
-- available.
if settings.startup["bobmods-power-nuclear"].value == true then

   data:extend({
         {
            type = "recipe",
            name = "logistic-reactor-2",
            result = "logistic-reactor-2",
            icon = "__logistics-reactor-next__/graphics/icons/logistic-reactor-recipe.png",
            icon_size = 32,
            energy_required = 12,
            enabled = false,
            ingredients =
               {
                  {"nuclear-reactor-2", 1},
                  {"logistic-chest-passive-provider-2", 1},
                  {"logistic-chest-requester-2", 1},
               },
            requester_paste_multiplier = 1
         },
         {
            type = "recipe",
            name = "logistic-reactor-3",
            result = "logistic-reactor-3",
            icon = "__logistics-reactor-next__/graphics/icons/logistic-reactor-recipe.png",
            icon_size = 32,
            energy_required = 16,
            enabled = false,
            ingredients =
               {
                  {"nuclear-reactor-3", 1},
                  {"logistic-chest-passive-provider-3", 1},
                  {"logistic-chest-requester-3", 1},
               },
            requester_paste_multiplier = 1
         }
   })

end
