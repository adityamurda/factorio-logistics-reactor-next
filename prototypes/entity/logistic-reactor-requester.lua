data:extend({util.merge{data.raw["logistic-container"]["logistic-chest-requester"],
  {
     name = "logistic-reactor-requester",
     inventory_size = 1,
     order = "z",
     flags = {"not-deconstructable", "placeable-player", "player-creation"}
  }
}})

if settings.startup["bobmods-power-nuclear"].value == true then

data:extend({util.merge{data.raw["logistic-container"]["logistic-chest-requester-2"],
  {
     name = "logistic-reactor-requester-2",
     inventory_size = 1,
     order = "z",
     flags = {"not-deconstructable", "placeable-player", "player-creation"}
  }
}})

data:extend({util.merge{data.raw["logistic-container"]["logistic-chest-requester-3"],
  {
     name = "logistic-reactor-requester-3",
     inventory_size = 1,
     order = "z",
     flags = {"not-deconstructable", "placeable-player", "player-creation"}
  }
}})

end
