data:extend({util.merge{data.raw.reactor["nuclear-reactor"],
  {
     name = "logistic-reactor",
     minable = {mining_time = 0.5, result = "logistic-reactor"}
  }
}})

if settings.startup["bobmods-power-nuclear"].value == true then

data:extend({util.merge{data.raw.reactor["nuclear-reactor-2"],
  {
     name = "logistic-reactor-2",
     minable = {mining_time = 0.5, result = "logistic-reactor-2"}
  }
}})

data:extend({util.merge{data.raw.reactor["nuclear-reactor-3"],
  {
     name = "logistic-reactor-3",
     minable = {mining_time = 0.5, result = "logistic-reactor-3"}
  }
}})

data.raw.reactor["logistic-reactor"].next_upgrade = "logistic-reactor-2"
data.raw.reactor["logistic-reactor-2"].next_upgrade = "logistic-reactor-3"

end
