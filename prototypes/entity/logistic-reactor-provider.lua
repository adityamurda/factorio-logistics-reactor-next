data:extend({util.merge{data.raw["logistic-container"]["logistic-chest-passive-provider"],
  {
     name = "logistic-reactor-passive-provider",
     inventory_size = 1,
     order = "z",
     flags = {"not-deconstructable", "placeable-player", "player-creation"}
  }
}})

if settings.startup["bobmods-power-nuclear"].value == true then

data:extend({util.merge{data.raw["logistic-container"]["logistic-chest-passive-provider-2"],
  {
     name = "logistic-reactor-passive-provider-2",
     inventory_size = 1,
     order = "z",
     flags = {"not-deconstructable", "placeable-player", "player-creation"}
  }
}})

data:extend({util.merge{data.raw["logistic-container"]["logistic-chest-passive-provider-3"],
  {
     name = "logistic-reactor-passive-provider-3",
     inventory_size = 1,
     order = "z",
     flags = {"not-deconstructable", "placeable-player", "player-creation"}
  }
}})

end
