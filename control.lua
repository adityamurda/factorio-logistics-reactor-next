reactor_count = 0
reactors_counted = false

local requester_map = {
   ["logistic-reactor"] = "logistic-reactor-requester",
   ["logistic-reactor-2"] = "logistic-reactor-requester-2",
   ["logistic-reactor-3"] = "logistic-reactor-requester-3"
}

local provider_map = {
   ["logistic-reactor"] = "logistic-reactor-passive-provider",
   ["logistic-reactor-2"] = "logistic-reactor-passive-provider-2",
   ["logistic-reactor-3"] = "logistic-reactor-passive-provider-3"
}


--------------------------------------------------------------------------------
-- Auxiliary
--------------------------------------------------------------------------------
function printTable(t)
   for k,v in pairs(t) do
      print(k, v)
   end
end

function info(msg)
   print("info[logistic-reactors]: " .. msg)
end

function isLogisticReactor(event)
   return requester_map[event.entity.name] ~= nil
end

function isLogisticReactorCreated(event)
   return requester_map[event.created_entity.name] ~= nil
end

function addToGlobalTable(entity)
   table.insert(global.logistic_reactors, entity)
   reactor_count = reactor_count + 1
end

function isSameEntityByPosition(e1, e2)
   return e1.position.x == e2.position.x and e1.position.y == e2.position.y
end

function removeFromGlobalTable(entity)
   for i, v in ipairs(global.logistic_reactors) do
      if notNil(v.entity, "position") then
         if isSameEntityByPosition(v.entity, entity) then
            table.remove(global.logistic_reactors, i)
            break
         end
      end
   end
   reactor_count = reactor_count - 1
end

function replaceGlobalTable(x)
   global.logistic_reactors = x
   countReactors()
end


function constructReactor(e)
   local s = e.surface
   local X = e.position.x
   local Y = e.position.y
   local f = e.force

   -- attach requester chest, set request to fuel cell
   local req = s.create_entity {
      name = requester_map[e.name],
      position = {X-2,Y+2},
      force=f
   }
   req.destructible = false
   req.minable = false

   -- attach passive provider chest
   local prov = s.create_entity {
      name = provider_map[e.name],
      position = {X+2,Y+2},
      force=f
   }
   prov.destructible = false
   prov.minable = false

   return {entity = e, req = req, prov = prov}
end

-- Spawn logistics interface when a reactor is built. Related events:
-- * on_built_entity
-- * on_robot_built_entity.
function builtEntity(event)
   if isLogisticReactorCreated(event) then
      reactor = constructReactor(event.created_entity)
      addToGlobalTable(reactor)
   end
end

-- if the reactor is valid, this will always succeed
function requesterForReactor(e)
   local X = e.position.x
   local Y = e.position.y
   return e.surface.find_entities_filtered {
      position = {X-2, Y+2},
      name = requester_map[e.name] -- "logistic-reactor-requester"
   }[1]
end

-- if the reactor is valid, this will always succeed
function passiveProviderForReactor(e)
   local X = e.position.x
   local Y = e.position.y
   return e.surface.find_entities_filtered {
      position = {X+2, Y+2},
      name = provider_map[e.name] -- "logistic-reactor-provider"
   }[1]
end

function destroyReactor(e, req, prov)
   -- destroy requester chest for this reactor
   if req ~= nil then
      req.destroy()
   end

   -- destroy passive provider chest for this reactor
   if prov ~= nil then
      prov.destroy()
   end
end

-- isEmptyItemStack :: LuaItemStack -> IO Bool
function isEmptyItemStack(i)
   return not i.valid_for_read
end

-- isEmptyItemStack :: LuaItemStack -> IO Bool
function needsRefueling(i)
   return isEmptyItemStack(i)
end

-- isEmptyItemStack :: LuaItemStack -> IO Bool
function needsWasteDisposal(i)
   return not isEmptyItemStack(i)
end


-- transferAllFromInventories :: LuaInventory -> LuaInventory -> IO Bool
-- * false: transfer failed
-- * true: transfer succeeded
--
-- failure modes:
-- * src inventory is invalid
-- * dest inventory is invalid
-- * dest inventory is full: abort halfway
function transferItemsFromInventories(src, dest)
   local srcContents = src.get_contents()
   local destContents = dest.get_contents()

   for k,v in pairs(srcContents) do
      local numInserted = dest.insert({name = k, count = v})
      if numInserted == 0 then
         return false
      end
      src.remove({name = k, count = v})
   end

   return true
end


function notNil(class, var)
   value = false
   pcall (function()
         if class[var] then
			value = true
         end
   end)
   return value
end

function countReactors()
   -- sync these periodically, just in case
   local count = 0
   for _,_ in ipairs(global.logistic_reactors) do
      count = count + 1
   end
   return count
end


--------------------------------------------------------------------------------
-- Handlers
--------------------------------------------------------------------------------
function onInit()
   if not global.logistic_reactors then
      global.logistic_reactors = {}
   end

   if not global.logistic_reactor_meta_valid then
      global.logistic_reactor_meta_valid = false
   end
end

function onLoad()
   reactor_count = countReactors()
   info("map contains " .. tostring(reactor_count) .. " logistic reactors")
end

function playerMinedReactor(event)
   if isLogisticReactor(event) then
      local e = event.entity
      local player = game.players[event.player_index]

      local req = requesterForReactor(e)
      local prov = passiveProviderForReactor(e)

      -- transfer inventories' contents to player
      local req_invent = req.get_inventory(1)
      local prov_invent = prov.get_inventory(1)
      local player_invent = player.get_main_inventory()
      if transferItemsFromInventories(req_invent, player_invent) then
         if transferItemsFromInventories(prov_invent, player_invent) then
            destroyReactor(e, req, prov)
            removeFromGlobalTable(e)
         else
            info("couldn't mine provider - player inventory full")
         end
      else
         info("couldn't mine requester - player inventory full")
      end
   end
end

function robotMinedReactor(event)
   if isLogisticReactor(event) then
      local e = event.entity
      local robot = event.robot

      local req = requesterForReactor(e)
      local prov = passiveProviderForReactor(e)

      -- transfer inventories' contents to robot
      -- transfer inventories' contents to player
      local robot_invent = robot.get_inventory(defines.inventory.robot_cargo)
      local req_invent = req.get_inventory(1)
      local prov_invent = prov.get_inventory(1)
      transferItemsFromInventories(req_invent, robot_invent)
      transferItemsFromInventories(prov_invent, robot_invent)

      destroyReactor(e, req, prov)
      removeFromGlobalTable(e)
   end
end

-- Destroy logistics interface when a reactor is destroyed
function reactorDied(event)
   if isLogisticReactor(event) then
      local e = event.entity
      local req = requesterForReactor(e)
      local prov = passiveProviderForReactor(e)
      destroyReactor(e, req, prov)
      removeFromGlobalTable(e)
   end
end

-- Moves fuel from the request chest to the fuel slot and removes
-- waste from the waste slot to the passive provider chest
function refuelReactorsAndEliminateWaste()

   -- if there are no reactors in the reactor table, exit early
   if reactor_count == 0 then
      return
   end

   for _, r in ipairs(global.logistic_reactors) do
      local reactor = r.entity
      local req = r.req
      local prov = r.prov

      -- if this reactor and its attached storages are still valid on
      -- this tick, continue. assumes that all operations complete
      -- atomically within a single tick.
      if reactor.valid and req.valid and prov.valid then
         local req_invent = req.get_inventory(1)
         local prov_invent = prov.get_inventory(1)

         -- if the reactor is out of fuel, pull from the attached request
         -- chest. this assumes that the only item the request chest will
         -- ever contain is a valid fuel cell for the reactor.
         local reactor_fuel_invent = reactor.get_fuel_inventory()
         if needsRefueling(reactor_fuel_invent[1]) then
            local req_fuel = req_invent[1]
            if req_fuel.valid_for_read then
               local req_fuel_left = req_fuel.count
               if req_fuel_left > 0 then
                  local num_inserted = reactor_fuel_invent.insert({
                        name = req_fuel.name,
                        count = 1
                  })
                  if num_inserted > 0 then
                     req_invent.remove( { name = req_fuel.name, count = num_inserted } )
                  end
               end
            end
         end

         -- move used fuel cells from the waste slot to the attached
         -- passive chest.
         local reactor_waste_invent = reactor.get_burnt_result_inventory()
         if needsWasteDisposal(reactor_waste_invent[1]) then
            local waste_stack = reactor_waste_invent[1]
            local waste_amount = waste_stack.count
            local num_inserted = prov_invent.insert(waste_stack)
            if num_inserted > 0 then
               reactor_waste_invent.remove(waste_stack)
            end
         end

      end -- end: entities valid
   end -- end: global reactor scan
end

-- a hack to populate the global reactor table once per game load
function countReactorsOnMap()
   if reactors_counted then
      return
   end

   local reactors_table = {}

   -- base reactors
   local reactors = game.surfaces[1].find_entities_filtered({
         name="logistic-reactor"
   })

   -- bob's reactors
   local bobsReactors2 = game.surfaces[1].find_entities_filtered({
         name="logistic-reactor-2"
   })

   local bobsReactors3 = game.surfaces[1].find_entities_filtered({
         name="logistic-reactor-3"
   })

   for _,ent in pairs(reactors) do
      local prov = passiveProviderForReactor(ent)
      local req = requesterForReactor(ent)
      local toInsert = {entity = ent, req = req, prov = prov}
      table.insert(reactors_table, toInsert)
   end

   for _,ent in pairs(bobsReactors2) do
      local prov = passiveProviderForReactor(ent)
      local req = requesterForReactor(ent)
      local toInsert = {entity = ent, req = req, prov = prov}
      table.insert(reactors_table, toInsert)
   end

   for _,ent in pairs(bobsReactors3) do
      local prov = passiveProviderForReactor(ent)
      local req = requesterForReactor(ent)
      local toInsert = {entity = ent, req = req, prov = prov}
      table.insert(reactors_table, toInsert)
   end

   replaceGlobalTable(reactors_table)
   reactors_counted = true
   global.logistic_reactor_meta_valid = true
   info("found " .. tostring(reactor_count) .. " reactors")
end

--------------------------------------------------------------------------------
-- Event Handlers Registration
--------------------------------------------------------------------------------
script.on_load(onLoad)
script.on_init(onInit)

-- every 10 seconds (default, configurable), check to see if reactors
-- need to request more fuel or if they need to send off waste
-- products.
local ticksTilUpdate =
   settings.startup["logistics-reactor-next-refuel-seconds"].value * 60
script.on_nth_tick(ticksTilUpdate, refuelReactorsAndEliminateWaste)

-- bug-fix trivia: having two on-nth-tick handlers registered for the
-- same tick effectively voids out those registered earlier. so, in
-- version 3.0.0 of this mod, refueling never happened if it was left
-- at the default of "every 10 seconds" because 10 * 60 == 600 which
-- was when `countReactorsOnMap` was scheduled to happen.
--
-- adding +1 here makes it so that it's impossible for
-- `refuelReactorsAndEliminateWaste` to ever overlap with
-- `countReactorsOnMap`.
script.on_nth_tick(601, countReactorsOnMap)

-- when building a logistic reactor, by player or bot, attach logistic
-- storages
script.on_event(defines.events.on_built_entity, builtEntity)
script.on_event(defines.events.on_robot_built_entity, builtEntity)

-- when deconstructing a logistic reactor (or it dies), properly
-- remove attached logistic storages, transfering contents to player
-- or bot if appropriate
script.on_event(defines.events.on_pre_player_mined_item, playerMinedReactor)
script.on_event(defines.events.on_entity_died, reactorDied)
script.on_event(defines.events.on_robot_mined_entity, robotMinedReactor)
