# Logistic Reactors Next

| logistic-reactors-next   | 0.4.0.0                           |
| ------------------------ | --------------------------------- |
| Maintainer               | Allele Dev (allele.dev@gmail.com) |
| Copyright                | Copyright (C) 2019 Allele Dev     |
| License                  | GPL-3                             |
| Factorio Version         | 0.17.x                            |
| Mod Page                 | [link](https://mods.factorio.com/mod/logistics-reactor-next) |

Deliver nuclear fuel to reactors via the logistics network. Makes
automated reactor grids possible!

An update of illysune's / jammerammer's 0.15 mod, [Logistic
Reactors](https://mods.factorio.com/mod/logistics-reactor).

It adds a new reactor type, logistic reactor. On placement, spawns
attached requester and passive provider chests. These are configured
to request fuel, dispense fuel, and remove waste.

With support for:

* [Bob's Power](https://mods.factorio.com/mods/Bobingabout/bobpower)

## Screenshots

* 4x4 Reactor Core

![4x4 Reactor Core](/images/reactor-core.png)

* Reactor System by Day

![Reactor at Day Time](/images/reactor-full-view-day.png)

* Reactor System by Night

![Reactor at Night Time](/images/reactor-full-view-night.png)

* Reactor Recipe

![Reactor Recipe](/images/reactor-recipe.png)

* Reactor Tech

![Reactor Tech](/images/reactor-research.png)
